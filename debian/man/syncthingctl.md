# syncthingtray(1) -- Tray application for Syncthing

## SYNOPSIS

`syncthingctl` [options]   
`syncthingctl` `--help`

## DESCRIPTION

Commandline interface for Syncthing.

To see a full list of command line options run `syncthingctl --help`.

